NG_DEBUG= Core.class()

require "lfs"

function NG_DEBUG:init()
	NG_DEBUG.LOGFILE = nil
end

function NG_DEBUG:SETUP()
	NG_DEBUG.LOGPATH1 = "/sdcard/Moosader/KingdomMath/kingdom-debug.log"
	NG_DEBUG.LOGPATH2 = "|D|kingdom-debug.log"
	
	NG_DEBUG.LOGPATH = NG_DEBUG.LOGPATH1
	NG_DEBUG.LOGFILE = io.open( NG_DEBUG.LOGPATH, "w" )
	
	success = true
	
	if ( NG_DEBUG.LOGFILE == nil ) then
		print( NG_DEBUG.LOGPATH1 .. " failed to load" )
		NG_DEBUG.LOGPATH = NG_DEBUG.LOGPATH2
		NG_DEBUG.LOGFILE = io.open( NG_DEBUG.LOGPATH, "w" )
		if ( NG_DEBUG.LOGFILE == nil ) then
			print( NG_DEBUG.LOGPATH2 .. " failed to load" )
			success = false
		end
	end
	
	if ( success == true ) then
		print( "Debug log path: " .. NG_DEBUG.LOGPATH )
	end
end

function NG_DEBUG:TEARDOWN()
	if ( NG_DEBUG.LOGFILE ~= nil ) then
		NG_DEBUG.LOGFILE:close()
	end
end

function NG_DEBUG:LOG( location, type, text )
	if ( type == "clutter" ) then return end
	time = os.time()
	
	-- Display the timestamp and code location
	locationLength = 75
	outputString = time .. "\t" .. type .." <" .. location .. ">"
	
	-- Space between location and debug message
	if ( string.len( outputString ) < locationLength ) then
		for i = 0, locationLength - string.len( outputString ), 1 do
			outputString = outputString .. " "
		end
	end
	
	textLength = 150
	outputString = outputString .. text
	
	if ( string.len( text ) < textLength ) then
		for i = 0, textLength - string.len( text ), 1 do
			outputString = outputString .. " "
		end
	end
	
	if ( NG_DEBUG.LOGFILE ~= nil ) then
		NG_DEBUG.LOGFILE:write( outputString .. "\n" )
	end
end