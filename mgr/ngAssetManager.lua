NG_ASSET_MANAGER = Core.class()

function NG_ASSET_MANAGER:init() end

function NG_ASSET_MANAGER:SETUP()
	NG_ASSET_MANAGER.TEXTURES = {}
	NG_ASSET_MANAGER.FONTS = {}
	NG_ASSET_MANAGER.SOUNDS = {}
	NG_ASSET_MANAGER.MUSIC = {}
	NG_ASSET_MANAGER.SOUND_CHANNEL = nil
end

-- Textures
-- key		path
function NG_ASSET_MANAGER:REGISTER_TEXTURE( options )
	-- key, path
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:REGISTER_TEXTURE", "clutter", 
		"Add texture asset \"" .. options.key .. "\" at path \"" .. options.path .. "\"" )
	NG_ASSET_MANAGER.TEXTURES[ options.key ] = Texture.new( options.path )
end

function NG_ASSET_MANAGER:GET_TEXTURE( key )
	if ( NG_ASSET_MANAGER.TEXTURES[ key ] == nil ) then
		NG_DEBUG:LOG( "NG_ASSET_MANAGER:GET_TEXTURE", "error", "Texture asset \"" .. key .. "\" does not exist" )
		return nil
	end
	
	return NG_ASSET_MANAGER.TEXTURES[ key ]
end

function NG_ASSET_MANAGER:CLEAR_TEXTURES()
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:CLEAR_TEXTURES", "info", "Function begin" )
	NG_ASSET_MANAGER.TEXTURES = {}    
end

function NG_TEXTURE( key )
	return NG_ASSET_MANAGER:GET_TEXTURE( key )
end

-- Fonts
-- key		path		size
function NG_ASSET_MANAGER:REGISTER_FONT( options )
	-- key, path, size
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:REGISTER_FONT", "clutter", 
		"Add font asset \"" .. options.key .. "\" at path \"" .. options.path .. "\" of size " .. options.size )
	NG_ASSET_MANAGER.FONTS[ options.key ] = TTFont.new( options.path, options.size )
end

function NG_ASSET_MANAGER:GET_FONT( key )
	if ( NG_ASSET_MANAGER.FONTS[ key ] == nil ) then
		NG_DEBUG:LOG( "NG_ASSET_MANAGER:GET_FONT", "error", "Font asset \"" .. key .. "\" does not exist" )
		return nil
	end
	
	return NG_ASSET_MANAGER.FONTS[ key ]
end

function NG_ASSET_MANAGER:CLEAR_FONTS()
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:CLEAR_FONTS", "info", "Function begin" )
	NG_ASSET_MANAGER.FONTS = {}    
end

function NG_FONT( key )
	return NG_ASSET_MANAGER:GET_FONT( key )
end

-- Audio
-- key		path		isMusic
function NG_ASSET_MANAGER:REGISTER_SOUND( options )
	local music = "false"
	if ( options.isMusic ) then music = "true" end
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:REGISTER_SOUND", "clutter", 
		"Add audio asset \"" .. options.key .. "\" at path \"" .. options.path .. "\" is music? " .. music )
		
	if ( options.isMusic ) then
		NG_ASSET_MANAGER.MUSIC[ options.key ] = Sound.new( options.path )
	else
		NG_ASSET_MANAGER.SOUNDS[ options.key ] = Sound.new( options.path )
	end
end

function NG_ASSET_MANAGER:PLAY_SOUND( key )
	if ( NG_ASSET_MANAGER.SOUNDS[ key ] ~= nil ) then
		NG_ASSET_MANAGER.SOUNDS[ key ]:play()
	end
	
	if ( key == "button-pressed" ) then
		--application:vibrate( 100 )
	end
end

function NG_ASSET_MANAGER:PLAY_MUSIC( key )
	if ( NG_ASSET_MANAGER.SOUND_CHANNEL ~= nil ) then
		NG_ASSET_MANAGER.SOUND_CHANNEL:stop()
	end
	
	NG_ASSET_MANAGER.SOUND_CHANNEL = NG_ASSET_MANAGER.MUSIC[ key ]:play()
	NG_ASSET_MANAGER.SOUND_CHANNEL:setLooping( true )
end

function NG_ASSET_MANAGER:CLEAR_SOUNDS()
	NG_DEBUG:LOG( "NG_ASSET_MANAGER:CLEAR_SOUNDS", "info", "Function begin" )
	NG_ASSET_MANAGER.MUSIC = {}
	NG_ASSET_MANAGER.SOUNDS = {}
end