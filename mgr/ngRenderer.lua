NG_RENDERER = Core.class()

function NG_RENDERER:init() end

function NG_RENDERER:SETUP()
    NG_DEBUG:LOG( "NG_RENDERER:SETUP", "info", "Initialize state manager" )
    NG_RENDERER.LAYERS = {}
    NG_RENDERER.DRAWABLES = {}
end

-- Only add ngImage, ngLabel, or ngButton
function NG_RENDERER:ADD_DRAWABLES( key, item )
    NG_DEBUG:LOG( "NG_RENDERER:ADD_DRAWABLES", "clutter", "Key: \"" .. key .. "\"" )
    NG_RENDERER.DRAWABLES[ key ] = item
end

function NG_RENDERER:GET_DRAWABLE( key )
    return NG_RENDERER.DRAWABLES[ key ]
end

function NG_RENDERER:UNDRAW_DRAWABLE( key )
    NG_DEBUG:LOG( "NG_RENDERER:UNDRAW_DRAWABLE", "info", "Key: \"" .. key .. "\"" )
	
	if ( NG_RENDERER.DRAWABLES[ key ] ~= nil ) then
		NG_RENDERER.DRAWABLES[ key ]:Cleanup()
	else
		NG_DEBUG:LOG( "NG_RENDERER:UNDRAW_DRAWABLE", "info", "Key \"" .. key .. "\" doesn't exist!" )
	end
end

function NG_RENDERER:CLEAR_DRAWABLES()
    NG_RENDERER:CLEAR()
    NG_RENDERER.DRAWABLES = {}
end

function NG_RENDERER:LAYER( index )
    if ( NG_RENDERER.LAYERS[ index ] == nil ) then
        NG_RENDERER.LAYERS[ index ] = Viewport.new()
    end

    return NG_RENDERER.LAYERS[ index ]
end

function NG_RENDERER:CLEAR()
    NG_DEBUG:LOG( "NG_RENDERER:CLEAR", "info", "Function begin, layer count: " .. #NG_RENDERER.LAYERS )

    for dk, drawable in pairs( NG_RENDERER.DRAWABLES ) do
        drawable:Cleanup()
    end

    for dl, layer in pairs( NG_RENDERER.LAYERS ) do
        if ( stage:contains( layer ) ) then
            stage:removeChild( layer )
        end
    end
end

function NG_RENDERER:DRAW()
    NG_RENDERER:CLEAR()

    for key, drawable in pairs( NG_RENDERER.DRAWABLES ) do
        drawable:Draw()
    end

    for key, layer in pairs( NG_RENDERER.LAYERS ) do
        stage:addChild( layer )
    end
end
