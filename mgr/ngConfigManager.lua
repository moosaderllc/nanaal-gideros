NG_CONFIG_MANAGER = Core.class()

function NG_CONFIG_MANAGER:init() end

function NG_CONFIG_MANAGER:SETUP()
	NG_CONFIG_MANAGER.SETTINGS = {}
	
	NG_CONFIG_MANAGER.SETTINGS[ "WIDTH" ] = application:getLogicalHeight()
	NG_CONFIG_MANAGER.SETTINGS[ "HEIGHT" ] = application:getLogicalWidth()
	
	print( NG_CONFIG_MANAGER.SETTINGS[ "WIDTH" ] .. ", " .. NG_CONFIG_MANAGER.SETTINGS[ "HEIGHT" ] )
end

function NG_CONFIG_MANAGER:SET( key, value )
	if ( type( value ) == "string" ) then
		NG_DEBUG:LOG( "NG_CONFIG_MANAGER:SET", "info", "Set \"" .. key .. "\" to \"" .. value .. "\"" )
	else
		NG_DEBUG:LOG( "NG_CONFIG_MANAGER:SET", "info", "Set \"" .. key .. "\" to data type \"" .. type( value ) .. "\"" )
	end
    NG_CONFIG_MANAGER.SETTINGS[ key ] = value
end

function NG_CONFIG_MANAGER:GET( key )
    return NG_CONFIG_MANAGER.SETTINGS[ key ]
end

function NG_CONFIG( key )
	return NG_CONFIG_MANAGER:GET( key )
end
