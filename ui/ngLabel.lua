-- Create a label:
--		self.lblTitle = NewLabel.new( {
--			x = 200, y = 200,
--			text = "Car Seat Safety Game",
--			font = AssetsManager.fonts[ "header" ],
--			color = 0xff0000
--		} )
-- Options: x, y, text, center (true, false), font, color. (Color is 0xRRGGBB, you can use GIMP to get color codes)
--
-- Draw a button:
-- self.lblTitle:Draw()

ngLabel = Core.class()

function ngLabel:init( options )
	-- Initialize member variables first
	self.text = {}
	self.font = nil
	self.x = nil
	self.y = nil
	self.textString = {}
	self.color = nil
	self.center = nil
	self.maxWidth = nil
	self.drawSurface = nil
	self.class = "label"
	
	self:Setup( options )
end

function ngLabel:GetClass() 
	return self.class 
end

function ngLabel:Cleanup()
	if ( self.text == nil ) then return end
	
	for key, item in pairs( self.text ) do
		if ( self.drawSurface:contains( item ) ) then self.drawSurface:removeChild( item ) end
	end	
end

function ngLabel:Setup( options )
	if ( options.drawSurface ~= nil ) then
		self.drawSurface = options.drawSurface
    else
        self.drawSurface = stage
	end

	if ( options.x ~= nil and options.y ~= nil ) then
		self.x = options.x
		self.y = options.y
	end	
	
	if ( options.center ~= nil ) then
		self.center = options.center
	else
		self.center = false
	end
	
	if ( options.color ~= nil ) then
		self.color = options.color
	else
		self.color = 0x000000 -- black (0x RR GG BB)
	end
	
	if ( options.font ~= nil ) then
		self.font = options.font
	end
	
	if ( options.maxWidth ~= nil ) then 
		self.maxWidth = options.maxWidth
	end
		
	if ( options.text ~= nil and options.font ~= nil ) then
		self.textString = options.text
		self:SetText( self.textString )
	end
end

function ngLabel:SetText( newText )
	self:Cleanup()
	
	self.textString = newText
	
	-- Clear old
	for key, item in pairs( self.text ) do
		self.text[ key ] = nil
	end
	
	if ( self.maxWidth == nil ) then
		self.maxWidth = NG_CONFIG( "WIDTH" )
	end

	local partialLine = newText --string.sub( newText, begin_str, end_str )
	local line = TextField.new( self.font, partialLine )
	
	if ( line:getWidth() > self.maxWidth ) then
		line:setScaleX( self.maxWidth / line:getWidth() ) -- just scale it
	end
	
	line:setTextColor( self.color )
	if ( self.x ~= nil and self.y ~= nil ) then
		line:setPosition( self.x, self.y )--self.y + ( i * line:getHeight() ) )
	end
	
	if ( self.center ) then 
		--self.text:setAnchorPoint( 0.5, 0.5 )
		local spriteWidth = line:getWidth()
		local giderosWidth = application:getLogicalHeight()
		local centerX = giderosWidth / 2 - spriteWidth / 2
		line:setX( centerX )
	end
	
	table.insert( self.text, line )
	
	self:Draw( self.drawSurface )
end

function ngLabel:Draw()
	if ( self.text ~= nil ) then
		for key, item in pairs( self.text ) do
			self.drawSurface:addChild( item )
		end
	end
end

function ngLabel:SetPosition( newx, newy )
	self.x = newx
	self.y = newy
	
	NG_DEBUG:LOG( "ngLabel:SetPosition", "clutter", "Amount of text in label: " .. #self.text ) -- location, type, text
	
	for key, item in pairs( self.text ) do
	
		try {
			function()
				-- This moves every text item to the same location.
				-- TODO: Change this
				item:setPosition( self.x, self.y )
			end,
		
			catch {
				function( error )
					print( error )
				end
			}
		}
	
	end
end

function ngLabel:SetRotation( angle, leftPosition )
	for key, item in pairs( self.text ) do
	
		try {
			function()
				-- This moves every text item to the same location.
				-- TODO: Change this
				item:setRotation( angle )

                if ( angle == 90 ) then
                    item:setPosition( self.y, self.x + leftPosition )
                end
			end,
		
			catch {
				function( error )
					print( error )
				end
			}
		}
	
	end
end

