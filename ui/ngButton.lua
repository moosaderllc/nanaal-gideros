-- Create a button:
--  self.btnPlay = NewButton.new( {
--      background = AssetsManager.textures[ "button_bg" ],
--      x = 10, y = 100,
--      text = "Play",
--      font = AssetsManager.fonts[ "main" ]
--      } )
-- Options: background, x, y, text, font, textColor, center, scale
--
-- Draw a button:
-- self.btnPlay:Draw()
--
-- Check if button was clicked:
--  if ( self.btnPlay:IsClicked( event ) ) then
--      print( "Clicked play!" )
--  end

ngButton = Core.class()

function ngButton:init( options )
    -- Initialize member variables first

    self.x = nil
    self.y = nil
    self.textColor = nil
    self.textY = nil
    self.center = nil
    self.scale = nil
    self.drawSurface = nil
    self.class = "button"

    self.label = nil
    self.image = nil

    self:Setup( options )
end

function ngButton:GetClass() 
    return self.class 
end

function ngButton:Cleanup()
	if ( self.label ~= nil ) then
		self.label:Cleanup()
	end
	
	if ( self.image ~= nil ) then
		self.image:Cleanup()
	end
end

function ngButton:Draw()
	if ( self.image ~= nil ) then
		self.image:Draw()
	end
	
	if ( self.label ~= nil ) then
		self.label:Draw()
	end
end

function ngButton:Setup( options )
    if ( options.drawSurface ~= nil ) then
        self.drawSurface = options.drawSurface
    else
        self.drawSurface = stage
    end
    
    if ( options.x ~= nil and options.y ~= nil ) then
        self.x = options.x
        self.y = options.y
    end

    if ( options.scale ~= nil ) then
        self.scale = options.scale
    else
        self.scale = 1.0
    end

    if ( options.center ~= nil ) then
        self.center = options.center
    else
        self.center = false
    end

    -- Set up image
    if ( options.background ~= nil ) then
        self.image = ngImage.new( {
            drawSurface = self.drawSurface,
            x = self.x, y = self.y,
            scaleX = self.scale, scaleY = self.scale, center = self.center,
            background = options.background
            } )

        self.width = self.image.background:getWidth()
        self.height = self.image.background:getHeight()

        if ( self.x ~= nil and self.y ~= nil ) then
            self.image:SetPosition( self.x, self.y )
        end
    end

    -- Set up label
    if ( options.text ~= nil and options.font ~= nil ) then
        -- Label position
        self.textX = self.x + ( self.width / 2 )
        self.textY = self.y + ( self.height / 2 )

        if ( options.textX ~= nil ) then
            self.textX = self.x + options.textX
        end

        if ( options.textY ~= nil ) then
            self.textY = self.y + options.textY
        end

        if ( options.textColor ~= nil ) then
            self.textColor = options.textColor
        else
            self.textColor = 0x000000 -- black (0x RR GG BB)
        end
        
        self.label = ngLabel.new( {
            drawSurface = self.drawSurface,
            x = self.textX, y = self.textY,
            color = self.textColor,
            text = options.text,
            font = options.font
        } )
    end
    
    if ( options.style ~= nil ) then
        self.style = options.style
        -- TODO: Should put this effect in the image, not the button
        self.image.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
        
        self.animateTimer = 0
        self.animateMax = 80
    end
end

function ngButton:Update()
    if ( self.style ~= nil and self.style == "blink" ) then
        self.animateTimer = self.animateTimer + 1
        if ( self.animateTimer >= self.animateMax ) then
            self.animateTimer = 0
        end
        
        if ( self.animateTimer > self.animateMax / 2 ) then
            self.image.background:setAlpha( 0.5 )
        else
            self.image.background:setAlpha( 1.0 )
        end
    end
end

function ngButton:ChangeBackground( options )
    NG_DEBUG:LOG( "ngButton:ChangeBackground", "info", "Function begin" )

    self.image:Cleanup()
    self.image:ChangeImage( options.background, self.drawSurface )
    self.width = self.image.background:getWidth()
    self.height = self.image.background:getHeight()
    self:Draw()
end

function ngButton:ChangeText( options )
    self.label:SetText( options.text )
    self:Draw()
end

function ngButton:IsClicked( event )
    return self.image.background:hitTestPoint( event.x, event.y )
end

function ngButton:MouseOver( event )
    return self.image.background:hitTestPoint( event.x, event.y )
end

function ngButton:GetPosition()
    return self.image.background:getPosition()
end

function ngButton:SetPosition( newx, newy )
    self.x = newx
    self.y = newy
    self.image:SetPosition( self.x, self.y )

    if ( self.label ~= nil ) then
        self.textX = self.x + ( self.width / 2 )
        self.textY = self.y + ( self.height / 2 )
        self.label:SetPosition( self.textX, self.textY )
    end
end

function ngButton:SetRotation( angleDeg, leftPosition )
    -- Rotate elements
    self.background:setRotation( { angle = angleDeg } )
    
    if ( self.text ~= nil ) then
        self.text:setRotation( angleDeg )
    end

    -- Change positions
    if ( angleDeg == 90 ) then        
        self.background:setPosition(self.y, self.x + leftPosition)
                
        if ( self.text ~= nil ) then
            self.text:setPosition(self.y, self.x + leftPosition)
        end
    end
end

