-- Create an image:
--		self.imgLogo = NewImage.new( {
--		background = AssetsManager.textures[ "title_logo" ],
--		x = 600, y = 200,
--		scaleX = 0.7, scaleY = 0.7
--		} )
-- Options: background, x, y, center, scaleX, scaleY (scale is 1.0 for 100%, 0.5 for 50%, etc.)
--
-- Draw a button:
-- self.imgLogo:Draw()

ngImage = Core.class()

function ngImage:init( options )
	-- Initialize member variables first
	self.background = nil
	self.x = nil
	self.y = nil
	self.scaleX = nil
	self.scaleY = nil
	self.center = nil
    self.drawSurface = nil
	self.style = nil
	self.class = "image"
	
	self:Setup( options )
end

function ngImage:GetClass() 
	return self.class 
end

function ngImage:Cleanup()
	if ( self.drawSurface:contains( self.background ) ) then self.drawSurface:removeChild( self.background ) end
end

function ngImage:Setup( options )
	if ( options.drawSurface ~= nil ) then
		self.drawSurface = options.drawSurface
    else
        self.drawSurface = stage
	end
    
	if ( options.x ~= nil and options.y ~= nil ) then
		self.x = options.x
		self.y = options.y
	end	
	
	if ( options.center ~= nil ) then
		self.center = options.center
	else
		self.center = false
	end
	
	if ( options.scale ~= nil ) then
		self.scale = options.scale
	else
		self.scale = 1.0
	end
	
	if ( options.scaleX ~= nil ) then
		self.scaleX = options.scaleX
	else
		self.scaleX = 1
	end
	
	if ( options.scaleY ~= nil ) then
		self.scaleY = options.scaleY
	else
		self.scaleY = 1
	end
	
	if ( options.background ~= nil ) then			
		self.background = Bitmap.new( options.background ) 
		if ( self.center ) then self.background:setAnchorPoint( 0.5, 0.5 ) 
		else self.background:setAnchorPoint( 0, 0 ) end	
		
		self.width = self.background:getWidth()
		self.height = self.background:getHeight()
	
		if ( options.anchorX ~= nil and options.anchorY ~= nil ) then
			self.background:setAnchorPoint( options.anchorX, options.anchorY )
		end
		
		if ( self.x ~= nil and self.y ~= nil ) then
			self.background:setPosition( self.x, self.y )
			self.background:setScaleX( self.scaleX )
			self.background:setScaleY( self.scaleY )
		end
	end
	
	if ( options.style ~= nil ) then
		self.style = options.style
		self.background:addEventListener( Event.ENTER_FRAME, self.Update, self )
		
		self.animateTimer = 0
		self.animateMax = 80
	end
end

function ngImage:Update()
	if ( self.style ~= nil and self.style == "blink" ) then
		self.animateTimer = self.animateTimer + 1
		if ( self.animateTimer >= self.animateMax ) then
			self.animateTimer = 0
		end
		
		if ( self.animateTimer > self.animateMax / 2 ) then
			self.background:setAlpha( 0.5 )
		else
			self.background:setAlpha( 1.0 )
		end
	end
end

function ngImage:Draw()
	if ( self.background ~= nil ) then
		self.drawSurface:addChild( self.background )
	end
end

function ngImage:ChangeImage( newTexture, surface )
	if ( self.drawSurface:contains( self.background ) ) then		self.drawSurface:removeChild( self.background )		end

	self.background:setTexture( newTexture )

    if ( self.x ~= nil and self.y ~= nil ) then
        self.background:setPosition( self.x, self.y )
    end
    
    if ( self.scaleX ~= nil and self.scaleY ~= nil ) then
        self.background:setScaleX( self.scaleX )
        self.background:setScaleY( self.scaleY )		
    end
    
    self:Draw()
end

function ngImage:SetPosition( newx, newy )
	self.x = newx
	self.y = newy
	self.background:setPosition( self.x, self.y )
end

function ngImage:GetPosition()
    return self.background:getPosition()
end

function ngImage:SetRotation( options )
	self.background:setAnchorPoint( 0.5, 0.5 )
    self.background:setRotation( options.angle )

    if ( options.angle == 90 and options.leftPosition ~= nil ) then
        self.background:setPosition(self.y, self.x + options.leftPosition)
    end
end

